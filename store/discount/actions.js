export default {
  async fetchList({ commit, rootState }) {
    try {
      const response = await this.$axios.get(`/discounts`, {
        headers: {
          Authorization: 'Bearer ' + rootState.user.token
        }
      })
      await commit('SET_VIEWING', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  },
  async fetchListShop({ commit, rootState }) {
    try {
      const response = await this.$axios.get(
        `/discounts/shop/${rootState.user.currentUser.shopId}`,
        {
          headers: {
            Authorization: 'Bearer ' + rootState.user.token
          }
        }
      )
      await commit('SET_VIEWING', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  },
  async fetchDetail({ state, commit }) {
    try {
      const detail = state.viewing.find((obj) => obj.id == state.selectId)
      await commit('SET_DETAIL', detail)
    } catch (error) {
      console.log(error)
      alert('Fetch detail get error')
    }
  }
}
