export default {
  async fetchShopOrders({ state, commit, rootState }) {
    try {
      const response = await this.$axios.get(
        `/orders?pageNo=${state.query.page}&pageSize=${state.query.size}&search=${state.query.search}&orderType=${state.query.status}`,
        {
          headers: {
            Authorization: 'Bearer ' + rootState.user.token
          }
        }
      )
      await commit('SET_SHOP_ORDER', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  }
}
