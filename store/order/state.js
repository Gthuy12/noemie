export default () => ({
  currentUser: null,
  products: {},
  shopOrders: {},
  query: {
    search: '',
    page: 1,
    size: 5,
    status: 0
  }
})
