export default {
  async fetchList({ state, commit }) {
    try {
      console.log('da vao day')
      const response = await this.$axios.get(
        `/categories?pageNo=${state.query.pageNo}&pageSize=${state.query.pageSize}`,
        {
          headers: {
            Authorization: 'Bearer ' + state.token
          }
        }
      )
      await commit('SET_VIEWING', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  },
  async fetchDetail({ state, commit }) {
    try {
      const detail = state.viewing.data.find((obj) => obj.id == state.selectId)
      await commit('SET_DETAIL', detail)
    } catch (error) {
      alert('Fetch detail get error')
    }
  }
}
