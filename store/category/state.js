export default () => ({
  viewing: {},
  query: {
    name: '',
    pageNo: 1,
    pageSize: 5
  },
  selectId: null,
  selected: {}
})
