export default () => ({
  currentUser: null,
  viewing: {},
  query: {
    search: '',
    page: 1,
    size: 5,
    status: 0,
    role: 0
  },
  token: ''
})
