export default {
  async fetchList({ state, commit }) {
    try {
      const response = await this.$axios.get(
        `/users?pageNo=${state.query.page}&pageSize=${state.query.size}&search=${state.query.search}&role=${state.query.role}&status=${state.query.status}`,
        {
          headers: {
            Authorization: 'Bearer ' + state.token
          }
        }
      )
      await commit('SET_VIEWING', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  },
  async fetchListBanned({ state, commit }) {
    try {
      const response = await this.$axios.get(
        `/users?s={"status": { "$eq": "BANNED" }}&page=${state.query.page}&limit=${state.query.size}`,
        {
          headers: {
            Authorization: 'Bearer ' + state.token
          }
        }
      )
      await commit('SET_VIEWING', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  }
}
