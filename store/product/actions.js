export default {
  async fetchShopProducts({ state, commit, rootState }) {
    try {
      const response = await this.$axios.get(
        `/shops/products?pageNo=${state.query.page}&pageSize=${state.query.size}&search=${state.query.search}&status=${state.query.status}`,
        {
          headers: {
            Authorization: 'Bearer ' + rootState.user.token
          }
        }
      )
      await commit('SET_SHOP_PRODUCT', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  }
}
