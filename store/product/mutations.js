export default {
  SET_VIEWING(state, payload) {
    state.viewing = payload
  },
  SET_SHOP_PRODUCT(state, payload) {
    state.shopProducts = payload
  },
  SET_TOKEN(state, token) {
    state.token = token
  },
  SET_FILTER(state, filter) {
    state.query.search = filter.searchText
    state.query.status = filter.status
  },
  SET_PAGE_FILTER(state, page) {
    state.query.page = page
  },
  SET_LIMIT_FILTER(state, limit) {
    state.query.size = limit
  },
  RESET_FILTER(state) {
    state.query.search = ''
    state.query.page = 1
    state.query.size = 5
    state.query.status = 0
  }
}
