export default () => ({
  currentUser: null,
  products: {},
  shopProducts: {},
  query: {
    search: '',
    page: 1,
    size: 5,
    status: 0
  }
})
