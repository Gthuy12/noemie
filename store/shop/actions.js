export default {
  async fetchList({ state, commit }) {
    try {
      const response = await this.$axios.get(
        `/shops?pageNo=${state.query.page}&pageSize=${state.query.size}&search=${state.query.search}&status=${state.query.status}`,
        {
          headers: {
            Authorization: 'Bearer ' + state.token
          }
        }
      )
      await commit('SET_VIEWING', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  },
  async fetchListUnapproved({ state, commit }) {
    try {
      const response = await this.$axios.get(
        `/shops/unapproved?pageNo=${state.query.page}&pageSize=${state.query.size}`,
        {
          headers: {
            Authorization: 'Bearer ' + state.token
          }
        }
      )
      await commit('SET_UNAPPROVE_SHOP', response.data)
    } catch (error) {
      alert('Fetch API error')
    }
  }
}
