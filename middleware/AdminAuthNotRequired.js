export default ({ store, redirect }) => {
  if (store.state.user.currentUser) {
    if (
      store.state.user.currentUser.roles.includes(1) ||
      store.state.user.currentUser.roles.includes(2)
    ) {
      return redirect('/admin/users')
    }
  }
}
