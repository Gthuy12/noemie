export default ({ store, redirect }) => {
  try {
    if (
      !store.state.user.currentUser.roles.includes(1) &&
      !store.state.user.currentUser.roles.includes(2)
    ) {
      store.dispatch('logout')
      return redirect('/admin/auth/login')
    }
  } catch (error) {
    store.dispatch('logout')
    return redirect('/admin/auth/login')
  }
}
